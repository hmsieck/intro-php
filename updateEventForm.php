<?php 

//connect db variables
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "wdv341";
$sql="";


//form variables
$event_name = "";
$event_description = "";
$event_presenter = "";
$event_date = "";
$event_time = "";

$event_nameError = "";
$event_descriptionError = "";
$event_presenterError = "";
$event_dateError = "";
$event_timeError = "";

$validForm = false;

$updateEventID = $_GET['event_id'];

			//validation functions
			function validateName(){
				global $event_name, $event_nameError, $validForm; //bring in variables
				$event_nameError="";					   // clear the error message
				if(trim($event_name)=="") {						  //if the name is empty, show the error msg
					$validForm = false;
					$event_nameError = "Please Enter an Event Name";
				}
			}

			function validateDescription(){
				global $event_description, $event_descriptionError, $validForm;
				$event_descriptionError = "";

				if(trim($event_description == "")) {
					$validForm = false;
					$event_descriptionError = "Please Enter an Event Description";
				}
			}

			function validatePresenter(){
				global $event_presenter, $event_presenterError, $validForm;
				$event_PresenterError = "";

				if(trim($event_presenter == "")) {
					$validForm = false;
					$event_presenterError = "Please Enter a Presenter";
				}
			}

			function validateDate(){
				global $event_date, $event_dateError, $validForm;
				$event_dateError = "";

				if($event_date == "") {
					$validForm = false;
					$event_dateError = "Please Enter a Date";
				}
			}

			function validateTime(){
				global $event_time, $event_timeError, $validForm;
				$event_timeError = "";

				if($event_time == "") {
					$validForm = false;
					$event_timeError = "Please Enter a Time";
				}
			} 

//$updateEventID = 13;

	if(isset($_POST["submit"])) //if the form has been seen by the user
		{

			//get name value pairs from post variable
			$event_name = $_POST['event_name'];
			$event_description = $_POST['event_description'];
			$event_presenter = $_POST['event_presenter'];
			$event_date = $_POST['event_date'];
			$event_time = $_POST['event_time'];

		
			//switch valid form to true
			$validform = true;
		
			//run validation functions
		
			validateName();
			validateDescription();
			validatePresenter();
			validateDate();
			validateTime();
		
			if($validform) {
				
				//connect to DB
				$conn = new PDO("mysql:host=$servername;dbname=wdv341", $username, $password);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				
				$sql = "UPDATE wdv341_event SET event_name = '$event_name', event_description = '$event_description', event_presenter = '$event_presenter', event_date = '$event_date', event_time = '$event_time' WHERE event_id = '$updateEventID';";
				
				$stmt = $conn->prepare($sql);
				/*$stmt->bindParam(':event_name', $event_name);
				$stmt->bindParam(':event_description', $event_description);
				$stmt->bindParam(':event_presenter', $event_presenter);
				$stmt->bindParam(':event_date', $event_date);
				$stmt->bindParam(':event_time', $event_time);
				$stmt->bindParam(':event_id', $updateEventID;); */
				$stmt->execute();
				
				if($stmt) {
					$msg ="Thanks, the event has been updated";
				}
				else {
					$msg ="oops that didnt work..";
				}
				
				
			}
		
			else {
				$msg = "oops, an error occured.";
			}
			
		} //end true branch of submit

	else { // if the form has not yet been seen by the user.
		
		//connect to DB
		$conn = new PDO("mysql:host=$servername;dbname=wdv341", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//create sql SELECT string
		$sql = "SELECT event_name, event_description, event_presenter, event_date, event_time FROM wdv341_event WHERE event_id = $updateEventID";
		
		//PREPARE the SQL statement
		 $stmt = $conn->prepare($sql);
		  
		//EXECUTE the prepared statement
		$stmt->execute();		
		  
		//RESULT object contains an associative array
		$stmt->setFetchMode(PDO::FETCH_ASSOC);	
		  
		$row=$stmt->fetch(PDO::FETCH_ASSOC);
		
		$event_name = $row['event_name'];
		$event_description = $row['event_description'];
		$event_presenter = $row['event_presenter'];		
		$event_date = $row['event_date'];
		$event_time = $row['event_time'];
		
	}



?>

<!doctype HTML>

<html>
	<head>
		<title>PHP Events form w/ SQL Insert</title>	
		<?php
            //If the form was submitted and valid and properly put into database display the INSERT result message
			if($validForm)
			{
        ?>
      <h1><?php echo $msg ?></h1>
        
        <?php
			}
			else	//display form
			{
				echo $msg;
        ?>
		<style>

		#form{
			width:600px;
			background-color: #ffdab7;
		}

		.error	{
			color:red;
			font-style:italic;	
			}

		.honeypot {
					display: none;
				}
		</style>
	</head>

	<body>
		
		
		<div id="form">
		  <form id="updateEventForm" name="updateEventForm" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) . "?event_id=$updateEventID"; ?>">
		  <h1>Event Registration Form</h1>
		  <table width="587" border="0">
			  
			<tr>
			  <td width="117">Event Name:</td>
			  <td width="246"><input type="text" name="event_name" id="event_name" value="<?php echo trim($event_name); ?>"/></td>
			  <td width="210" class="error"> <?php echo $event_nameError ?> </td>
			</tr>
			  
			<tr>
			  <td>Event Description:</td>
				<td><textarea name="event_description" id="event_description"><?php echo trim($event_description); ?></textarea></td>
			  <td class="error"> <?php echo $event_descriptionError ?> </td>
			</tr>
			  
			<tr>
			  	<td>Event Presenter:</td>
			 	<td width="246"><input type="text" name="event_presenter" id="event_presenter" value="<?php echo $event_presenter; ?>"/></td>
			  	<td class="error"><?php echo $event_presenterError ?></td>
			</tr>
			
			<tr>
			  	<td>Event Date:</td>
			 	<td><input type="date" name="event_date" id="event_date" value="<?php echo $event_date; ?>"/></td>
			  	<td class="error"><?php echo $event_dateError ?></td>
			</tr>
			  
			<tr>
			  	<td>Event Time:</td>
			 	<td><input type="time" name="event_time" id="event_time" value="<?php echo $event_time; ?>"/></td>
			  	<td class="error"><?php echo $event_timeError ?></td>
			</tr>
			  
		  </table>
		  <p>
			<input type="submit" name="submit" id="button" value="Update" />
		  </p>
		</form>
			
		<?php } ?>

		</div>
		
	</body>
</html>
