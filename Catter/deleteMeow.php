<?php

	//connect to DB
	require "connectPDO.php";
		
	$delete_id = $_GET['meow_id']; //pulls specific event from the get variable

	
	$sql = "DELETE FROM wdv341_thoughts WHERE meow_id = $delete_id;"; 

	//echo $delete_id;
	//echo $sql;

	$stmt = $conn->prepare($sql);
	$stmt-> execute();
	
	

	if ($stmt) {
		$msg= "<p>Your meow was deleted successfully!</p>";
	}

	else {
		$msg= "<p>Oops, something went wrong.. please try again. $delete_id</p>";
	}
?>
<html>
<head>
	<title>Catter- PHP Portfolio</title>
	
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/solid.css" integrity="sha384-HTDlLIcgXajNzMJv5hiW5s2fwegQng6Hi+fN6t5VAcwO/9qbg2YEANIyKBlqLsiT" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/regular.css" integrity="sha384-R7FIq3bpFaYzR4ogOiz75MKHyuVK0iHja8gmH1DHlZSq4tT/78gKAa7nl4PJD7GP" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/fontawesome.css" integrity="sha384-8WwquHbb2jqa7gKWSoAwbJBV2Q+/rQRss9UXL5wlvXOZfSodONmVnifo/+5xJIWX" crossorigin="anonymous">




	<style>
		
		/* logo color: #c79100 */
		
		* {
			font-family: 'Lato', sans-serif;
			text-align: center;
		}
		
		h1 {
			text-align: center;
			margin-top: 2%;
		}
		
		table {
			width: 50%;
			margin: auto;
			min-width: 500px;
			max-width: 700px;
		}
		
		p {
			text-align: center;
		}
		
		td {
    		border-bottom: 1px solid #ddd;
			padding: 15px;
			text-align: left;
		}
		
		.profile {
			width: 10%;
			vertical-align: top;
		}
		
		.meow{
			width: 90%;
		}
		
		.button {
		  display: block;
			width: 70px;
			margin: auto;
			text-align: center;
		   border: 2px solid #c79200;
		   background: #c79200;
		   background: -webkit-gradient(linear, left top, left bottom, from(#c79200), to(#c79200));
		   background: -webkit-linear-gradient(top, #c79200, #c79200);
		   background: -moz-linear-gradient(top, #c79200, #c79200);
		   background: -ms-linear-gradient(top, #c79200, #c79200);
		   background: -o-linear-gradient(top, #c79200, #c79200);
		   background-image: -ms-linear-gradient(top, #c79200 0%, #c79200 100%);
		   padding: 10px 20px;
		   -webkit-border-radius: 9px;
		   -moz-border-radius: 9px;
		   border-radius: 9px;
		   -webkit-box-shadow: rgba(255,255,255,0.4) 0 0px 0, inset rgba(255,255,255,0.4) 0 0px 0;
		   -moz-box-shadow: rgba(255,255,255,0.4) 0 0px 0, inset rgba(255,255,255,0.4) 0 0px 0;
		   box-shadow: rgba(255,255,255,0.4) 0 0px 0, inset rgba(255,255,255,0.4) 0 0px 0;
		   text-shadow: #c79200 0 1px 0;
		   color: #ffffff;
		   font-size: 15px;
		   font-family: helvetica, serif;
		   text-decoration: none;
		   vertical-align: middle;
		   }
		.button:hover {
		   border: 2px solid #c79200;
		   text-shadow: #ffffff 0 1px 0;
		   background: #ffffff;
		   background-image: -ms-linear-gradient(top, #ffffff 0%, #ffffff 100%);
		   color: #c79200;
		   }
		.edit {
			float:right;
			padding-left: 1em;
			color: #136000;
		}

		.edit:hover {
			color:red;
		}
		
				/* Icon bar styles*/
		
		.icon-bar {
			width: 2.3em;
			background-color: #555;
			float: left;
			position: fixed;
			margin-left: -.5em;
		}

		.icon-bar a {
			display: block;
			text-align: center;
			transition: all 0.3s ease;
			color: white;
			font-size: 1.2em;
			padding-top: .5em;
			padding-bottom: .5em;
		}

		.icon-bar a:hover {
			background-color: #000;
		}

		.active {
			background-color: #c79100 !important;
		}
		
		body {
			min-width: 700px;
		}
		
		.header {
			width: 80%;
			max-width: 850px;
		}
	</style>
	
</head>
	
<body>
	<h1><img src="catterlogo.png" class="header"></h1>
	
			
						<!--Icon Bar -->
		<div class="icon-bar">
		  <a href="selectMeows.php" alt="home"><i class="fas fa-home"></i></a>
		  <a href="meowForm.php" class="active" alt="create meow"><i class="fas fa-paw"></i></a> 
		  <a href="meowContact.php" alt="contact"><i class="fa fa-envelope"></i></a> 
		  <a href="login.php" alt="login"><i class="fas fa-sign-in-alt"></i></a>
		</div>
	
	<div id="content">
		
	<h4><?php echo $msg ?></h4>	
	
	
	<a href="selectMeows.php">Let's go back to the timeline</a>
	</div>
	

	

</body>
</html>

