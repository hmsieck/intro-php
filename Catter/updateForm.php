<?php 

$meow_name = "";
$meow_message = "";

$meow_name_error = "";
$meow_message_error = "";

$validForm = false;

$update_meow_id = $_GET['meow_id'];

function validateName(){
	global $meow_name, $meow_name_error, $validForm; //bring in variables
	$meow_name_error="";					   // clear the error message
	if(trim($meow_name)=="") {						  //if the name is empty, show the error msg
		$validForm = false;
		$meow_name_error = "Please Enter a Name";
	}
}

function validateThought(){
	global $meow_message, $meow_message_error, $validForm; //bring in variables
	$meow_message_error="";					   // clear the error message
	if(trim($meow_message)=="") {						  //if the name is empty, show the error msg
		$validForm = false;
		$meow_message_error = "Please Enter a Name";
		
	}
}



if(isset($_POST["submit"]))
	{
		//get name value pairs from post variables
		$meow_name = htmlspecialchars($_POST['meow_name']);
		$meow_message = htmlspecialchars($_POST['meow_message']);
	
		//switch form to true
		$validForm = true;
		
		//run validation funx
		validateName();
		validateThought();
	
		if($validForm) {
			//connect to DB
			include "connectPDO.php";
			
			//prepare sql update string
			$sql = "UPDATE wdv341_thoughts SET meow_name='$meow_name', meow_message = '$meow_message' WHERE meow_id = '$update_meow_id';";
			
			//prepare statement & execute
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			
			if($stmt) {
					$msg ="<h3>Thanks, the meow has been edited!</h3> <p><a href='selectMeows.php'>Let's go back to the timeline</a></p>";
			}
			else {
				$msg ="oops, that didnt work..";
			}
		}
		else {
			$msg = "oops, an error occured.";
		}
} //end true branch of submit

else //if the form has not yet been seen by the user..
	{

		//connect to DB
		include "connectPDO.php";
	
		//create SQL select string
		$sql = "SELECT meow_name, meow_message FROM wdv341_thoughts WHERE meow_id = '$update_meow_id';";
	
		//prepare & execute
		$stmt = $conn->prepare($sql);
		$stmt->execute();
	
		//RESULT object contains an associative array
		$stmt->setFetchMode(PDO::FETCH_ASSOC);	
		  
		$row=$stmt->fetch(PDO::FETCH_ASSOC);
		
		$meow_name = $row['meow_name'];
		$meow_message = $row['meow_message'];
	
	}

?>

<!doctype HTML>

<html>
	<head>
		<title>iTHoughtThat</title>	
		
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/solid.css" integrity="sha384-HTDlLIcgXajNzMJv5hiW5s2fwegQng6Hi+fN6t5VAcwO/9qbg2YEANIyKBlqLsiT" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/regular.css" integrity="sha384-R7FIq3bpFaYzR4ogOiz75MKHyuVK0iHja8gmH1DHlZSq4tT/78gKAa7nl4PJD7GP" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/fontawesome.css" integrity="sha384-8WwquHbb2jqa7gKWSoAwbJBV2Q+/rQRss9UXL5wlvXOZfSodONmVnifo/+5xJIWX" crossorigin="anonymous">

		<style>

		#form	{
			width:600px;
			margin: auto;
		}

		.error	{
			color:red;
			font-style:italic;	
			}

		.honeypot {
					display: none;
				}
			
				* {
			font-family: 'Lato', sans-serif;
					text-align: center;
		}
		
		h1 {
			text-align: center;
			margin-top: 2%;
		}
		
		table {
			width: 50%;
			margin: auto;
			min-width: 500px;
			max-width: 700px;
		}
		
		p {
			text-align: center;
		}
		
		td {
    		border-bottom: 1px solid #ddd;
			padding: 15px;
			text-align: left;
		}
		
		
		.button {
		  display: block;
			width: 140px;
			margin: auto;
			text-align: center;
		   border: 2px solid #c79200;
		   background: #c79200;
		   background: -webkit-gradient(linear, left top, left bottom, from(#c79200), to(#c79200));
		   background: -webkit-linear-gradient(top, #c79200, #c79200);
		   background: -moz-linear-gradient(top, #c79200, #c79200);
		   background: -ms-linear-gradient(top, #c79200, #c79200);
		   background: -o-linear-gradient(top, #c79200, #c79200);
		   background-image: -ms-linear-gradient(top, #c79200 0%, #c79200 100%);
		   padding: 10px 20px;
		   -webkit-border-radius: 9px;
		   -moz-border-radius: 9px;
		   border-radius: 9px;
		   -webkit-box-shadow: rgba(255,255,255,0.4) 0 0px 0, inset rgba(255,255,255,0.4) 0 0px 0;
		   -moz-box-shadow: rgba(255,255,255,0.4) 0 0px 0, inset rgba(255,255,255,0.4) 0 0px 0;
		   box-shadow: rgba(255,255,255,0.4) 0 0px 0, inset rgba(255,255,255,0.4) 0 0px 0;
		   text-shadow: #c79200 0 1px 0;
		   color: #ffffff;
		   font-size: 15px;
		   font-family: helvetica, serif;
		   text-decoration: none;
		   vertical-align: middle;
		   }
		.button:hover {
		   border: 2px solid #c79200;
		   text-shadow: #ffffff 0 1px 0;
		   background: #ffffff;
		   background-image: -ms-linear-gradient(top, #ffffff 0%, #ffffff 100%);
		   color: #c79200;
		   }
		.form-align {
			display: inline;
    		vertical-align: top;
			}
			
					/* Icon bar styles*/
		
		.icon-bar {
			width: 2.3em;
			background-color: #555;
			float: left;
			position: fixed;
			margin-left: -.5em;
		}

		.icon-bar a {
			display: block;
			text-align: center;
			transition: all 0.3s ease;
			color: white;
			font-size: 1.2em;
			padding-top: .5em;
			padding-bottom: .5em;
		}

		.icon-bar a:hover {
			background-color: #000;
		}

		.active {
			background-color: #c79100 !important;
		}
		
		body {
			min-width: 700px;
		}
		
		.header {
			width: 80%;
			max-width: 850px;
		}
		</style>
	</head>

	<body>
			<h1><img src="catterlogo.png" class="header"></h1>
		
						<!--Icon Bar -->
		<div class="icon-bar">
		  <a href="selectMeows.php" alt="home"><i class="fas fa-home"></i></a>
		  <a href="meowForm.php" class="active" alt="create meow"><i class="fas fa-paw"></i></a> 
		  <a href="meowContact.php" alt="contact"><i class="fa fa-envelope"></i></a> 
		  <a href="login.php" alt="login"><i class="fas fa-sign-in-alt"></i></a>
		</div>
	
		<div id="content">
		
		<?php
            //If the form was submitted and valid and properly put into database display the INSERT result message
			if($validForm)
			{
        ?>
			
      	<p><?php echo $msg ?></p>
        
        <?php
			}
			else	//display form
			{
				echo $msg;
        ?>
		
		<div id="form">
		  <form id="updateForm" name="updateForm" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) . "?meow_id=$update_meow_id"; ?>">

			  <h4>Username: <input type="text" name="meow_name" id="meow_name" value="<?php echo trim($meow_name); ?>"/></h4>
			  <p class="error"><?php echo $meow_name_error ?></p>
			  
			  <h4><div class="form-align">Meow: </div><textarea name="meow_message" id="meow_message" rows="5" cols="70"><?php echo trim($meow_message); ?></textarea> </h4>
			  <p class="error"><?php echo $meow_message_error ?></p>

		  <p>
			<input type="submit" name="submit" class="button" value="Submit Meow" /> <br>
			<a href="selectMeows.php">Back to the timeline.</a>
		  </p>
		</form>
			<?php } ?>
		</div>
		
	</body>
</html>
