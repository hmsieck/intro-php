<?php

//emailer Class definition

class Emailer {					// define class name
	private $sendTo="";         //define properites-- same as variables 
	private $sentFrom="";
	private $emailSubject="";
	private $emailMsg="";
	
	
	public function __construct()		//constructor function -- start name with double underscore
	{
		
	}
	
	
	public function setSendTo($inSendTo) //define setters(inputs a value and sets it to a property/variable- change things)
	{
		$this->sendTo = $inSendTo;		//keyword: this->var = class/current object, object assignment, no space, assign incoming value
	}
	
	
	public function setSentFrom($inSentFrom)
	{
		$this->sentFrom = $inSentFrom;
	}
	
	
	public function setEmailSubject($inEmailSubject)
	{
		$this->emailSubject = $inEmailSubject;
	}
	
	
	public function setEmailMsg($inEmailMsg)
	{
		//echo $inEmailMsg;
		$inEmailMsg = htmlentities($inEmailMsg); 	//turns special characters into HTML characters/symbols, so it'll show up correctly.
		$inEmailMsg = wordwrap($inEmailMsg, 70, "\n"); //emails have to be wrapped at 70 char. per line
		$this->emailMsg = $inEmailMsg; 		
		//echo $inEmailMsg;
	}
	
	
	public function getSendTo()				//define getters, rarely have parameters, always have a return, return a value from a property
	{
		return $this->sendTo;
	}
	
	
	public function getSentFrom()
	{
		return $this->sentFrom;
	}
	
	
	public function getEmailSubject()
	{
		return $this->emailSubject;
	}
	
	
	public function getEmailMsg()
	{
		//echo $inEmailMsg;
		return $this->emailMsg;
	}
	
	
	public function sendEmail()			//define functional methods, puts all the pieces together
	{
		$headers = "From: $this->sentFrom" . "\r\n"; //builds headers
		//echo <"h2"Headers $headers</h2>";"
		
		return mail($this->sendTo, $this->emailSubject, $this->emailMsg, $headers); //returns true or false, runs mail function

	}
	
}