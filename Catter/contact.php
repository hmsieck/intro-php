<!doctype HTML>

<html>
	
	<head>
		<title>Contact Us | Catter</title>
		
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/solid.css" integrity="sha384-HTDlLIcgXajNzMJv5hiW5s2fwegQng6Hi+fN6t5VAcwO/9qbg2YEANIyKBlqLsiT" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/regular.css" integrity="sha384-R7FIq3bpFaYzR4ogOiz75MKHyuVK0iHja8gmH1DHlZSq4tT/78gKAa7nl4PJD7GP" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/fontawesome.css" integrity="sha384-8WwquHbb2jqa7gKWSoAwbJBV2Q+/rQRss9UXL5wlvXOZfSodONmVnifo/+5xJIWX" crossorigin="anonymous">




	<style>
		
		#form	{
			width:600px;
			margin: auto;
		}

		.error	{
			color:red;
			font-style:italic;	
			}

		.honeypot {
					display: none;
				}
			
				* {
			font-family: 'Lato', sans-serif;
					text-align: center;
		}
		
		h1 {
			text-align: center;
			margin-top: 2%;
		}
		
		table {
			width: 50%;
			margin: auto;
			min-width: 500px;
			max-width: 700px;
		}
		
		p {
			text-align: center;
		}
		
		td {
    		border-bottom: 1px solid #ddd;
			padding: 15px;
			text-align: left;
		}
		
		
		.button {
			width: 140px;
			margin: auto;
			text-align: center;
		   border: 2px solid #c79200;
		   background: #c79200;
		   padding: 10px 20px;
		   -webkit-border-radius: 9px;
		   -moz-border-radius: 9px;
		   border-radius: 9px;
		   text-shadow: #c79200 0 1px 0;
		   color: #ffffff;
		   font-size: 15px;
		   font-family: helvetica, serif;
		   text-decoration: none;
		   vertical-align: middle;
		   }
		.button:hover {
		   border: 2px solid #c79200;
		   text-shadow: #ffffff 0 1px 0;
		   background: #ffffff;
		   color: #c79200;
		}
		form-align {
			display: inline;
    		vertical-align: top;
			}
		
		/* Icon bar styles*/
		
		.icon-bar {
			width: 2.3em;
			background-color: #555;
			float: left;
			position: fixed;
			margin-left: -.5em;
		}

		.icon-bar a {
			display: block;
			text-align: center;
			transition: all 0.3s ease;
			color: white;
			font-size: 1.2em;
			padding-top: .5em;
			padding-bottom: .5em;
		}

		.icon-bar a:hover {
			background-color: #000;
		}

		.active {
			background-color: #c79100 !important;
		}
		
		body {
			min-width: 700px;
		}
		
		.header {
			width: 80%;
			max-width: 850px;
		}
	</style>
	
	</head>

	<body>
		
		<!-- header image -->
		<h1><img src="catterlogo.png" class="header"></h1>
		
							<!--Icon Bar -->
		<div class="icon-bar">
		  <a href="meowHome.php" alt="home"><i class="fas fa-home"></i></a>
		  <a href="contact.php" class="active" alt="contact"><i class="fa fa-envelope"></i></a> 
		  <a href="login.php" alt="logout"><i class="fas fa-sign-in-alt"></i></a>
		  <a href="signup.php" alt="signup"><i class="fas fa-user-plus"></i></a>
		</div>
		

		<!--Contact Form-->
		<div class="contactform">
		<form id="contact" name="contact" method="post" action="contactFormHandler.php">  
			<h1>Contact Us:</h1>
			
			<p>Questions? Comments? Suggestions? We'd love to hear from you!</p>
			<p>Fill out the form below and we'll be in touch shortly. Meow.</p>
			
			<p><strong>Name: </strong>
				<input type="text" name="name" id="name"/>
			</p>
			
			<p><strong>Email Address: </strong>
				<input type="text" name="email" id="email"/>
			</p>
			
			<div class="form-align"><strong>Message: </strong></div>
				<textarea name="message" id="message" rows="5" cols="70"></textarea>

			<p>
				<input type="submit" name="button" id="button" value="Submit" class="button"/>
				<input type="reset" name="button2" id="button2" value="Reset" class="button"/>
			</p>

		</form>
		</div>
		

	</body>
	
</html>