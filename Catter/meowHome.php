<?php 

include 'connectPDO.php';

$sql = "SELECT meow_id, meow_name, meow_message, meow_date FROM wdv341_thoughts";

$stmt = $conn->prepare($sql);
$stmt->execute();

$updateForm = "updateForm.php?meow_id=$meow_id";

?>

<html>
<head>
	<title>Catter- PHP Portfolio</title>
	
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/solid.css" integrity="sha384-HTDlLIcgXajNzMJv5hiW5s2fwegQng6Hi+fN6t5VAcwO/9qbg2YEANIyKBlqLsiT" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/regular.css" integrity="sha384-R7FIq3bpFaYzR4ogOiz75MKHyuVK0iHja8gmH1DHlZSq4tT/78gKAa7nl4PJD7GP" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/fontawesome.css" integrity="sha384-8WwquHbb2jqa7gKWSoAwbJBV2Q+/rQRss9UXL5wlvXOZfSodONmVnifo/+5xJIWX" crossorigin="anonymous">




	<style>
		
		/* logo color: #c79100 */
		
		* {
			font-family: 'Lato', sans-serif;
		}
		
		h1 {
			text-align: center;
			margin-top: 2%;
		}
		
		table {
			width: 50%;
			margin: auto;
			min-width: 500px;
			max-width: 700px;
		}
		
		p {
			text-align: center;
		}
		
		td {
    		border-bottom: 1px solid #ddd;
			padding: 15px;
			text-align: left;
		}
		
		.profile {
			width: 10%;
			vertical-align: top;
		}
		
		.meow{
			width: 90%;
		}
		
		.button {
		  display: block;
			width: 70px;
			margin: auto;
			text-align: center;
		   border: 2px solid #c79200;
		   background: #c79200;
		   padding: 10px 20px;
		   -webkit-border-radius: 9px;
		   -moz-border-radius: 9px;
		   border-radius: 9px;
		   text-shadow: #c79200 0 1px 0;
		   color: #ffffff;
		   font-size: 15px;
		   font-family: helvetica, serif;
		   text-decoration: none;
		   vertical-align: middle;
		   }
		.button:hover {
		   border: 2px solid #c79200;
		   text-shadow: #ffffff 0 1px 0;
		   background: #ffffff;
		   color: #c79200;
		   }
		.edit {
			float:right;
			padding-left: 1em;
			color: #136000;
		}

		.edit:hover {
			color:red;
		}
		
				/* Icon bar styles*/
		
		.icon-bar {
			width: 2.3em;
			background-color: #555;
			float: left;
			position: fixed;
			margin-left: -.5em;
		}

		.icon-bar a {
			display: block;
			text-align: center;
			transition: all 0.3s ease;
			color: white;
			font-size: 1.2em;
			padding-top: .5em;
			padding-bottom: .5em;
		}

		.icon-bar a:hover {
			background-color: #000;
		}

		.active {
			background-color: #c79100 !important;
		}
		
		body {
			min-width: 700px;
		}
		
		.header {
			width: 80%;
			max-width: 850px;
		}
	</style>
	
</head>
	
<body>
	<h1><img src="catterlogo.png" class="header"></h1>
	
					<!--Icon Bar -->
		<div class="icon-bar">
		  <a href="meowHome.php" class="active" alt="home"><i class="fas fa-home"></i></a>
		  <a href="contact.php" alt="contact"><i class="fa fa-envelope"></i></a> 
		  <a href="login.php" alt="logout"><i class="fas fa-sign-in-alt"></i></a>
		  <a href="signup.php" alt="signup"><i class="fas fa-user-plus"></i></a>
		</div>
	
	<div id="content">
		
		<p>Twitter who? Tweet what? At Catter the only thing we like about birds is catching em. </p>
		<?php
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$meow_id = $row["meow_id"];
			$meow_name = $row["meow_name"];
			$meow_message = $row["meow_message"];
			
		$table = "<table>";
		
			
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$meow_id = $row["meow_id"];
			$meow_date = $row["meow_date"];
			$table .= "<tr>";
			$table .= "<td class='profile'><img src='catlogo.jpg'></td>";
			$table .= "<td class='meow'> <strong>@</strong>". $row["meow_name"] . "<br><br>" . $row["meow_message"] . "<br><br>". date('m-d-Y, h:ma', strtotime($meow_date)) . "</td>";
			$table .= "</tr>";
			
		}
			$table .= "</table>";
		
		?>
		
		
		
		
		<?php echo $table;?>
		
	</div>
	
	<?php
		}//close while loop
		$row->close();
		$conn->close();	//Close the database connection	
	?>
	

</body>
</html>