<?php 

$user_name = "";
$user_password = "";

$user_name_error = "";
$user_password_error = "";

$validForm = false;


function validateUserName(){
	global $user_name, $user_name_error, $validForm; //bring in variables
	$user_name_error="";					   // clear the error message
	if(trim($user_name)=="") {						  //if the name is empty, show the error msg
		$validForm = false;
		$user_name_error = "Please Enter a User Name";
	}
}

function validateUserPassword(){
	global $user_password, $user_password_error, $validForm; //bring in variables
	$user_password_error="";					   // clear the error message
	if(trim($user_password)=="") {						  //if the name is empty, show the error msg
		$validForm = false;
		$user_password_error = "Please Enter a Password!";
		
	}
}



if(isset($_POST["submit"]))
	{
		$user_name = htmlspecialchars($_POST['user_name']);
		$user_password = htmlspecialchars($_POST['user_password']);
	
		$validForm = true;
		
		validateUserName();
		validateUserPassword();
	
	}

else 
	{

	}

?>

<!doctype html>
<html>

<head>
	
	<title>Sign Up! | Catter</title>
	
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/solid.css" integrity="sha384-HTDlLIcgXajNzMJv5hiW5s2fwegQng6Hi+fN6t5VAcwO/9qbg2YEANIyKBlqLsiT" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/regular.css" integrity="sha384-R7FIq3bpFaYzR4ogOiz75MKHyuVK0iHja8gmH1DHlZSq4tT/78gKAa7nl4PJD7GP" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/fontawesome.css" integrity="sha384-8WwquHbb2jqa7gKWSoAwbJBV2Q+/rQRss9UXL5wlvXOZfSodONmVnifo/+5xJIWX" crossorigin="anonymous">
	
	<style>
		#form	{
			width:600px;
			margin: auto;
		}

		.error	{
			color:red;
			font-style:italic;	
			}

		.honeypot {
					display: none;
				}
			
				* {
			font-family: 'Lato', sans-serif;
					text-align: center;
		}
		
		h1 {
			text-align: center;
			margin-top: 2%;
		}
		
		table {
			width: 50%;
			margin: auto;
			min-width: 500px;
			max-width: 700px;
		}
		
		p {
			text-align: center;
		}
		
		td {
    		border-bottom: 1px solid #ddd;
			padding: 15px;
			text-align: left;
		}
		
		
		.button {
		  display: block;
			width: 140px;
			margin: auto;
			text-align: center;
		   border: 2px solid #c79200;
		   background: #c79200;
		   padding: 10px 20px;
		   -webkit-border-radius: 9px;
		   -moz-border-radius: 9px;
		   border-radius: 9px;
		   text-shadow: #c79200 0 1px 0;
		   color: #ffffff;
		   font-size: 15px;
		   font-family: helvetica, serif;
		   text-decoration: none;
		   vertical-align: middle;
		   }
		.button:hover {
		   border: 2px solid #c79200;
		   text-shadow: #ffffff 0 1px 0;
		   background: #ffffff;
		   color: #c79200;
		   }
		.form-align {
			display: inline;
    		vertical-align: top;
			}
			
					/* Icon bar styles*/
		
		.icon-bar {
			width: 2.3em;
			background-color: #555;
			float: left;
			position: fixed;
			margin-left: -.5em;
		}

		.icon-bar a {
			display: block;
			text-align: center;
			transition: all 0.3s ease;
			color: white;
			font-size: 1.2em;
			padding-top: .5em;
			padding-bottom: .5em;
		}

		.icon-bar a:hover {
			background-color: #000;
		}

		.active {
			background-color: #c79100 !important;
		}
		
		body {
			min-width: 700px;
		}
		
		.header {
			width: 80%;
			max-width: 850px;
		}
</style>
	
</head>
	
	<body>
		
		<h1><img src="catterlogo.png" class="header"></h1>
		
				<!--Icon Bar -->
		<div class="icon-bar">
		  <a href="meowHome.php" alt="home"><i class="fas fa-home"></i></a>
		  <a href="contact.php" alt="contact"><i class="fa fa-envelope"></i></a> 
		  <a href="login.php"  alt="logout"><i class="fas fa-sign-in-alt"></i></a>
		  <a href="signup.php" class="active" alt="signup"><i class="fas fa-user-plus"></i></a>
		</div>
	
		<h1> Register Below!</h1>
		
		<?php

			if ($validForm)			//If the form info is valid
			{ 
				include 'connectPDO.php';
				
				$sql = "INSERT INTO `wdv341_users` (`user_id`, `user_name`, `user_password`) VALUES (NULL, '$user_name', '$user_password');";
	
				$stmt = $conn->prepare($sql);
				$stmt->bindParam(':user_name', $user_name);
				$stmt->bindParam(':user_password', $user_password);
				$stmt->execute();
				echo  "<h3>Thank You!</h3>";
				echo "<a href='selectMeows.php'>Let's go back to the timeline!</a>";
				
			?>
				
				

			<?php
			}	//end the true branch of the form view area
			else
			{
		?>
		
		<div id="form">
		
			
			<form id="SignUp" name="SignUp" method="post" action="signup.php">
				
				<h4>Username: <input type="text" name ="user_name" id="user_name" value="<?php echo trim($user_name); ?>"></h4>
				<p class="error"><?php echo $user_name_error ?></p>
				
				<h4>Password: <input type="password" name ="user_password" id="user_password" value="<?php echo trim($user_password); ?>"></h4>
				<p class="error"><?php echo $user_password_error ?></p>
				
				<p>
					<input type="submit" name="submit" class="button" value="Sign Up!" /> <br>
					<a href="login.php">Let's Log In!</a>
					
					
				</p>
				
				
			</form>
			
		</div>
		
		<?php
	}	//end els
	   
	   ?>

	</body>

</html>