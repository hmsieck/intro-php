<?php
$inUserName = "";
$msg = "";
//set up session
session_start();
//if the page opens with a valid user...
if($_SESSION['validUser']== "yes")
{
	//set confirmation msg
	$msg = "Welcome back ".$inUsername."!";
}
//else, if page opens without a valid user...
else
{
	//if the page was reached by a submitted login form...
	if(isset($_POST["submit"]) )
	{
		//set username and password from form
		$inUsername = $_POST["loginUser"];
		$inPassword = $_POST["loginPass"];
		
		//connect to database
		include "connectPDO.php";
		
		//set up SQL SELECT query for username and password that were entered into form
		$sql = "SELECT user_name, user_password FROM wdv341_users WHERE user_name = '$inUsername' AND user_password = '$inPassword'";
		
		//run SELCT query
		$result = $conn->query($sql);
		
		//if the query retrieves 1 record...
		if($result)
		{
			if($result->rowCount() == 1)
			{
				//user is a valid user
				$_SESSION['validUser'] = "yes";
				//set confirmation msg
				header('location: selectMeows.php');

			}
			//else, if 0 or more than 1 records were found...
			else
			{
				//user is not a valid user
				$_SESSION['validUser'] = "no";
				//set error msg
				$msg = "There was a problem with your username or password. Please try again.";
			}
		}
	}
	//else, if the user needs to see the login form...
	else
	{
		
	}
	
}

?>
<!DOCTYPE html>
<html>
<head>
	
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/solid.css" integrity="sha384-HTDlLIcgXajNzMJv5hiW5s2fwegQng6Hi+fN6t5VAcwO/9qbg2YEANIyKBlqLsiT" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/regular.css" integrity="sha384-R7FIq3bpFaYzR4ogOiz75MKHyuVK0iHja8gmH1DHlZSq4tT/78gKAa7nl4PJD7GP" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/fontawesome.css" integrity="sha384-8WwquHbb2jqa7gKWSoAwbJBV2Q+/rQRss9UXL5wlvXOZfSodONmVnifo/+5xJIWX" crossorigin="anonymous">

<style>
		#form	{
			width:600px;
			margin: auto;
		}

		.error	{
			color:red;
			font-style:italic;	
			}

		.honeypot {
					display: none;
				}
			
				* {
			font-family: 'Lato', sans-serif;
					text-align: center;
		}
		
		h1 {
			text-align: center;
			margin-top: 2%;
		}
		
		table {
			width: 50%;
			margin: auto;
			min-width: 500px;
			max-width: 700px;
		}
		
		p {
			text-align: center;
		}
		
		td {
    		border-bottom: 1px solid #ddd;
			padding: 15px;
			text-align: left;
		}
		
		
		.button {
		  display: block;
			width: 140px;
			margin: auto;
			text-align: center;
		   border: 2px solid #c79200;
		   background: #c79200;
		   padding: 10px 20px;
		   -webkit-border-radius: 9px;
		   -moz-border-radius: 9px;
		   border-radius: 9px;
		   text-shadow: #c79200 0 1px 0;
		   color: #ffffff;
		   font-size: 15px;
		   font-family: helvetica, serif;
		   text-decoration: none;
		   vertical-align: middle;
		   }
		.button:hover {
		   border: 2px solid #c79200;
		   text-shadow: #ffffff 0 1px 0;
		   background: #ffffff;
		   color: #c79200;
		   }
		.form-align {
			display: inline;
    		vertical-align: top;
			}
			
					/* Icon bar styles*/
		
		.icon-bar {
			width: 2.3em;
			background-color: #555;
			float: left;
			position: fixed;
			margin-left: -.5em;
		}

		.icon-bar a {
			display: block;
			text-align: center;
			transition: all 0.3s ease;
			color: white;
			font-size: 1.2em;
			padding-top: .5em;
			padding-bottom: .5em;
		}

		.icon-bar a:hover {
			background-color: #000;
		}

		.active {
			background-color: #c79100 !important;
		}
		
		body {
			min-width: 700px;
		}
		
		.header {
			width: 80%;
			max-width: 850px;
		}
</style>
</head>
<body>
	
	
<div id ="container">
	
	
<h1><img src="catterlogo.png" class="header"></h1>
	
	
<h1><?php echo $msg?></h1>

<?php
//if the user is a valid user...
if($_SESSION['validUser'] == "yes")
{
	//show admin page
	header('location: login.php');
	
	$inUserName = $_GET["loginUser"];



	echo 
		"<div class='icon-bar'>
		  <a href='selectMeows.php' alt='home'><i class='fas fa-home'></i></a>
		  <a href='meowForm.php' alt='create meow' class='active'><i class='fas fa-paw'></i></a> 
		  <a href='meowContact.php' alt='contact'><i class='fa fa-envelope'></i></a> 
		  <a href='login.php' alt='login'><i class='fas fa-sign-in-alt'></i></a>
		</div>
	
<p><a href='logout.php'>Logout</a></p>";
			
?>
<?php
}
//else, if not a valid user...
else
{
	//show login form
?>

	
<h2>Please Log In</h2>
<form method="post" name="loginForm" action="login.php" id="form">
<p>Username: <input type="text" name="loginUser" /></p>
<p>Password: <input type="password" name="loginPass" /></p>
<p><input type="submit" name ="submit" value="Login"></p>
<a href="meowHome.php">Back to the home page..</a>
</form>
	
	
</div>
<?php
}
?>


</body>
</html>
