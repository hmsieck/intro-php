<?php

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "wdv341";
$sql="";

		
		$conn = new PDO("mysql:host=$servername;dbname=wdv341", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("SELECT event_id, event_name, event_description, event_presenter, event_date, event_time FROM wdv341_event");
		$stmt->execute();
			
		$table = "<table>";
		$table .= "<tr>";
		$table .= "<th>Event Name:</th><th>Event Description:</th><th>Event Presenter:</th><th>Event Date:</th><th>Event Time:</th><th>Update Event</th>";
		$table .= "</tr>";
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$eventId = $row["event_id"];
			$table .= "<tr>";
			$table .= "<td>". $row["event_name"] . "</td>";
			$table .= "<td>". $row["event_description"] . "</td>";
			$table .= "<td>". $row["event_presenter"] . "</td>";
			$table .= "<td>". $row["event_date"] . "</td>";
			$table .= "<td>". $row["event_time"] . "</td>";
			$table .= "<td>". "<a href='updateEventForm.php?event_id=$eventId'>Update</a> <br> <a href='deleteEvent.php?event_id=$eventId'>Delete</a>" . "</td>";
			$table .= "</tr>";

		}
		
		$table .= "</table>";
?>

<html>
<head>
	<title>WDV341 SELECT Example</title>
	
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

	<style>
		
		* {
			font-family: 'Lato', sans-serif;
		}
		
		h1 {
			text-align: center;
			margin-top: 2%;
		}
		
		table {
			width: 80%;
			margin: auto;
		}
		
		p {
			text-align: center;
		}

		th {
			background-color: #3d87ff;
			color: white;
			height: 50px;
			border-bottom: 1px solid #ddd;
			padding: 15px;
    		text-align: left;
		}
		
		td {
    		border-bottom: 1px solid #ddd;
			padding: 15px;
			text-align: left;
		}
		
		tr:hover {
			background-color: #c9dcff;
		}


	</style>
	
</head>
	
<body>
	<h1>WDV 341 Events Table</h1>
	<div id="content">
		<?php echo $table; ?>
		
	<p><a href="eventsForm.php">Create A New Event</a></p>
	</div>

</body>
</html>