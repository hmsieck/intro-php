<?php 

$event_name = "";
$event_description = "";
$event_presenter = "";
$event_date = "";
$event_time = "";

$event_nameError = "";
$event_descriptionError = "";
$event_presenterError = "";
$event_dateError = "";
$event_timeError = "";

$validForm = false;


function validateName(){
	global $event_name, $event_nameError, $validForm; //bring in variables
	$event_nameError="";					   // clear the error message
	if(trim($event_name)=="") {						  //if the name is empty, show the error msg
		$validForm = false;
		$event_nameError = "Please Enter an Event Name";
	}
}

function validateDescription(){
	global $event_description, $event_descriptionError, $validForm;
	$event_descriptionError = "";
	
	if(trim($event_description == "")) {
		$validForm = false;
		$event_descriptionError = "Please Enter an Event Description";
	}
}

function validatePresenter(){
	global $event_presenter, $event_presenterError, $validForm;
	$event_PresenterError = "";
	
	if(trim($event_presenter == "")) {
		$validForm = false;
		$event_presenterError = "Please Enter a Presenter";
	}
}

function validateDate(){
	global $event_date, $event_dateError, $validForm;
	$event_dateError = "";
	
	if($event_date == "") {
		$validForm = false;
		$event_dateError = "Please Enter a Date";
	}
}

function validateTime(){
	global $event_time, $event_timeError, $validForm;
	$event_timeError = "";
	
	if($event_time == "") {
		$validForm = false;
		$event_timeError = "Please Enter a Time";
	}
}


if(isset($_POST["submit"]))
	{
		$event_name = trim($_POST['event_name']);
		$event_description = trim($_POST['event_description']);
		$event_presenter = trim($_POST['event_presenter']);
		$event_date = $_POST['event_date'];
		$event_time = $_POST['event_time'];
	
		$validForm = true;
		
		
		validateName();
		validateDescription();
		validatePresenter();
		validateDate();
		validateTime();
	
		
	}

else 
	{

	}

?>

<!doctype HTML>

<html>
	<head>
		<title>PHP Events form w/ SQL Insert</title>	
		<style>

		#form	{
			width:600px;
			background-color: #ffdab7;
		}

		.error	{
			color:red;
			font-style:italic;	
			}

		.honeypot {
					display: none;
				}
		</style>
	</head>

	<body>
		
		<?php

			if ($validForm)			//If the form info is valid
			{ 
				include 'connectDB.php';
				
				
				echo  "<h3>Thank You!</h3>
				<p>Your <strong>$event_name</strong> event on <strong>$event_date</strong> has been registered!</p>";
				
			?>
				
				

			<?php
			}	//end the true branch of the form view area
			else
			{
		?>
		
		<div id="form">
		  <form id="EventRegistration" name="EventRegistration" method="post" action="eventsForm.php">
		  <h1>Event Registration Form</h1>
		  <table width="587" border="0">
			  
			<tr>
			  <td width="117">Event Name:</td>
			  <td width="246"><input type="text" name="event_name" id="event_name" value="<?php echo trim($event_name); ?>"/></td>
			  <td width="210" class="error"> <?php echo $event_nameError ?> </td>
			</tr>
			  
			<tr>
			  <td>Event Description:</td>
				<td><textarea name="event_description" id="event_description"><?php echo trim($event_description); ?></textarea></td>
			  <td class="error"> <?php echo $event_descriptionError ?> </td>
			</tr>
			  
			<tr>
			  	<td>Event Presenter:</td>
			 	<td width="246"><input type="text" name="event_presenter" id="event_presenter" value="<?php echo $event_presenter; ?>"/></td>
			  	<td class="error"><?php echo $event_presenterError ?></td>
			</tr>
			
			<tr>
			  	<td>Event Date:</td>
			 	<td><input type="date" name="event_date" id="event_date" value="<?php echo $event_date; ?>"/></td>
			  	<td class="error"><?php echo $event_dateError ?></td>
			</tr>
			  
			<tr>
			  	<td>Event Time:</td>
			 	<td><input type="time" name="event_time" id="event_time" value="<?php echo $event_time; ?>"/></td>
			  	<td class="error"><?php echo $event_timeError ?></td>
			</tr>
			  
		  </table>
		  <p>
			<input type="submit" name="submit" id="button" value="Register" />
		  </p>
		</form>
			<?php
	}	//end else branch for the View area
	?>
		</div>
		
	</body>
</html>
