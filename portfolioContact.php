<!doctype HTML>

<html>
	
	<head>
		<title>Holly Sieck Web Portfolio</title>
		
		<link href="https://fonts.googleapis.com/css?family=Cutive+Mono|Raleway:400,700" rel="stylesheet">
		<link rel="stylesheet" href="portfolioStyle.css">
		
		<style>
		
			body {
				background-image: none;
			}
			
			.contactform{
				text-align: center;
				line-height: 100%;
				padding-top: 100px;

			}
			
			#contactlogo {
				margin: auto;
				display: block;
				height: 100px;
				width: auto;
			}
			
			#message {
				width: 100%;
				height: 10em;
				display: block;
				margin:auto;
			}
			
		</style>
	
	</head>

	<body>
		<!--Navigation Bar-->
		<nav>
			<ul>
				<li class="navleft"><a href="#intro"><img src="profilepic.png" width="75" alt=""></a></li>
				<li class="navleft"><a href="#intro"> < hi. im holly./> </a></li>
				<li class="navright"><a href="#">{contact}</a></li>
				<li class="navright"><a href="http://portfolio.hollysieck.info/#projects">{projects}</a></li>
				<li class="navright"><a href="http://portfolio.hollysieck.info/#about">{about me}</a></li>
			</ul>
		</nav>
		
		
		<!--Contact Form-->
		<div class="contactform">
		<form id="contact" name="contact" method="post" action="contactFormHandler.php">  
			<h1>Contact Me:</h1>
			
			<p>fill out the form below and I will be in touch shortly &#9787;	</p>
			
			<p><strong>Name: </strong>
				<input type="text" name="name" id="name"/>
			</p>
			
			<p><strong>Email Address: </strong>
				<input type="text" name="email" id="email"/>
			</p>
			
			<p><strong>Message: </strong></p>
				<textarea name="message" id="message"></textarea>

			<p>
				<input type="submit" name="button" id="button" value="Submit" />
				<input type="reset" name="button2" id="button2" value="Reset" />
			</p>

		</form>
		</div>
		
		<!--footer-->
		
		<footer>
		<h1>thanks for visiting!</h1>
		<p>&copy;copyright hollysieck 2018 <img src="hslogosmall.png" id="contactlogo">
</p>
		</footer>
		
	</body>
	
</html>