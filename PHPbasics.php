<!DOCTYPE HTML>
<html>

<head><title>PHP Basics</title></head>
	
<body>
	
	<h1>PHP Basics</h1>
	
	<?php 
		$yourName = "Holly"; 
		echo "<h1> $yourName </h1>";
	?>
	
	<h2 id="h2tag"></h2>
	
	<script>
		<?php echo "document.getElementById('h2tag').innerHTML = '$yourName';"; ?>
	</script>
	
		<?php 
			$number1 = 1;
			$number2 = 5;
			$total = $number1+$number2;

			echo  "Value one: $number1";
			echo "<br>";
			echo  "Value two: $number2";
			echo "<br>";
			echo  "Total: $total";

		?>
	
	<script>
	
		<?php 
			echo "var lang = ['PHP', 'HTML', 'JavaScript'];";
			echo "document.write('<p>' + lang + '</p>')"
		?>
	
	</script>
	
</body>
</html>