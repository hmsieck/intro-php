<!doctype HTML>

<html>
	
	<head>
		<title>Holly Sieck Web Portfolio</title>
		
		<link href="https://fonts.googleapis.com/css?family=Cutive+Mono|Raleway:400,700" rel="stylesheet">
		<link rel="stylesheet" href="portfolioStyle.css">
		
		<style>
			
			.contactform{
				text-align: center;
				line-height: 100%;
				padding-top: 100px;

			}
			
			#contactlogo {
				margin: auto;
				display: block;
				height: 100px;
				width: auto;
			}
			
			#message {
				width: 100%;
				height: 10em;
				display: block;
				margin:auto;
			}
			
			.confirmation {
				padding-top: 550px;
				text-align: center;
			}
			
			
		</style>
	
		<?php
 
//get the values entered in the field and store them in a variable 
$inName = $_POST["name"];
$inEmail = $_POST["email"];
$inMessage = $_POST["message"];

include 'Emailer.php'; //link in Email Class 

//echo "<h1> Confirmation Email </h1>";
//send confirmation email to person who filled out form 

$confirmatonEmail = new Emailer(); //creating a new object from the email class 

$confirmatonEmail->setSendTo($inEmail);
//echo "<p> <strong>Send To:</strong> " . $confirmatonEmail->getSendTo() . "</p>";

$confirmatonEmail->setSentFrom("contact@hollysieck.info");
//echo "<p><strong>Sent From:</strong> " . $confirmatonEmail->getSentFrom() . "</p>";

$confirmatonEmail->setEmailSubject("Message Recieved - hollysieck.info");
//echo "<p> <strong>Email Subject:</strong> " . $confirmatonEmail->getEmailSubject() . "</p>";

$confirmatonEmail->setEmailMsg("Hello " . $inName . ". Your message has been sent, and I will be in contact with you shortly. Thanks, Holly ");
//echo "<p> <strong>Email Message: </strong>" .  $confirmatonEmail->getEmailMsg() . "</p>";
	
$confirmatonEmail->sendEmail(); //creates and sends an email

// send form information
//echo "<h1> Email with form info </h1>";

$formEmail = new Emailer(); 

$formEmail->setSendTo("hmsieck@dmacc.edu");
//echo "<p> <strong>Send To:</strong> " . $formEmail->getSendTo() . "</p>";

$formEmail->setSentFrom("contact@hollysieck.info");
//echo "<p> <strong>Sent From:</strong> " . $formEmail->getSentFrom() . "</p>";

$formEmail->setEmailSubject("New Contact Inquiry From hollysieck.info");
//echo "<p> <strong>Email Subject:</strong> " . $formEmail->getEmailSubject() . "</p>";

$formEmail->setEmailMsg("Contact Name: " . $inName . "; Contact Email: " . $inEmail . "; Message: " . $inMessage . ";");
//echo "<p> <strong>Message:</strong> " . $formEmail->getEmailMsg() . "</p>";

$formEmail->sendEmail(); //creates and sends the email 

?>



		
	</head>

	<body>
		<!--Navigation Bar-->
		<nav>
			<ul>
				<li class="navleft"><a href="#intro"><img src="profilepic.png" width="75" alt=""></a></li>
				<li class="navleft"><a href="#intro"> < hi. im holly./> </a></li>
				<li class="navright"><a href="#">{contact}</a></li>
				<li class="navright"><a href="http://portfolio.hollysieck.info/#projects">{projects}</a></li>
				<li class="navright"><a href="http://portfolio.hollysieck.info/#about">{about me}</a></li>
			</ul>
		</nav>
		
		
		<div class="confirmation">
		<?php echo "<h1>Thanks " . $inName . "! You're message has been sent!</h1>";?>
		<p><a href="http://portfolio.hollysieck.info/">Back to home page</a></p>
		</div>
		<!--footer-->
		
		<footer>
		<p>&copy;copyright hollysieck 2018</p>
		</footer>
		
	</body>
	
</html>