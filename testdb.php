<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "wdv341";

try {
	
	global $event_name, $event_description, $event_presenter, $event_date, $event_time;
	
    $conn = new PDO("mysql:host=$servername;dbname=wdv341", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully <br>"; 
	
	$stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time) 
    VALUES (:event_name, :event_description, :event_presenter, :event_date, :event_time)");
    $stmt->bindParam(':event_name', $event_name);
    $stmt->bindParam(':event_description', $event_description);
    $stmt->bindParam(':event_presenter', $event_presenter);
	$stmt->bindParam(':event_date', $event_date);
	$stmt->bindParam(':event_time', $event_time);

    // insert a row
    $event_name = "Pizza Party";
    $event_description = "Pizza party in PHP on monday night";
    $event_presenter = "Holly";
	$event_date = "2018-03-05";
	$event_time = "18:00";
    $stmt->execute();
	
	 echo "New records created successfully";
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
?>
