<?php 
//set up variables for every field:

$inName = "";
$inSSN = "";
$inResponse = "";

if(isset($_POST["submit"])) //if the form has been submitted do the following: 
{
	
	//get the name/value pairs from POST and store them in variables. 
	
	$inName = $_POST['inName'];
	$inSSN = $_POST['inSSN'];
	$inResponse = $_POST['inResponse'];
	
}

else //display the empty form 
{

}

?>
<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Form Validation Example</title>
<style>

#orderArea	{
	width:600px;
	background-color:#CF9;
}

.error	{
	color:red;
	font-style:italic;	
}
</style>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>Form Validation Assignment


</h2>
<div id="orderArea">
  <form id="form1" name="form1" method="post" action="createSelfPostingForm.php">
  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="inName" id="inName" size="40" value="<?php echo $inName; ?>"/></td>
      <td width="210" class="error"></td>
    </tr>
    <tr>
      <td>Social Security</td>
      <td><input type="text" name="inSSN" id="inSSN" size="40" value="<?php echo $inSSN; ?>" /></td>
      <td class="error"></td>
    </tr>
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="inResponse" id="inResponse1" value="Phone" <?php if ($inResponse == 'Phone') { echo "checked"; } ?> />
          Phone</label>
        <br>
        <label>
          <input type="radio" name="inResponse" id="inResponse2" value="Email" <?php if ($inResponse == 'Email') { echo "checked"; } ?> />
          Email</label>
        <br>
        <label>
          <input type="radio" name="inResponse" id="inResponse3" value="US Mail" <?php if ($inResponse == 'US Mail') { echo "checked"; } ?> />
          US Mail</label>
        <br>
      </p></td>
      <td class="error"></td>
    </tr>
  </table>
  <p>
    <input type="submit" name="submit" id="button" value="Register" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>
</div>
	
	<a href= "https://bitbucket.org/hmsieck/intro-php/src">View PHP code</a>

</body>
</html>