<!doctype HTML>
<html>

<head><title>PHP Functions</title></head>
	
<body>
	
	<h1>Assignment: PHP Functions</h1>
	
	
	<?php

	function formatTime() {
		$dateInput = mktime(2, 10, 2018);
		echo "February 10th, 2018: " . date("m/d/Y", $dateInput) . "</br>";
	}

	function formatInternationalTime() {
		$dateInputInternational = mktime(2, 10, 2018);
		echo "February 10th, 2018: " . date("d/m/Y", $dateInputInternational) . "</br>";
	}
	
	
	?>
	
	<p>1. Create a function that will accept a date input and format it into mm/dd/yyyy format.</p>
	<?php formatTime(); ?>
	
	<p>2. Create a function that will accept a date input and format it into dd/mm/yyyy format to use when working with international dates.</p>
	<?php formatInternationalTime(); ?>
	
	<?php
	
		function containDMACC() {
			$stringInput = " Hello World ";
			$find = "dmacc";
			$pos = strpos($stringInput, $find);

			if ($pos === false) {
				echo "The string " . "'" . $find . "'" . " was not found in the string " . "'" . trim($stringInput) . "' <br>";
			} else {
				echo "The string " . "'" . $find . "'" . " was found in the string " . "'" . $stringInput . "' <br>";
			}
		}
		function displayString() {
			$stringInput = " Hello World ";
			echo strtolower($stringInput) . " has ". strlen(trim($stringInput)) . " characters.";
			echo "<br>";
		}
	?>
	
	<p>3. Create a function that will accept a string input.  It will do the following things to the string:</p>
	<p>(a) Display whether the string contains "DMACC"</p>
	<?php containDMACC(); ?>
	<p>(b) Display number of characters in the string, trim white space, turn string to all lowercase:</p>
	<?php displayString(); ?>
	
	<?php 
	
		function formatNumber() {
			$number = 1234567890;
			echo number_format($number);
		}
	
		function formatUsCurrency() {
			$number = 123456;
			setlocale(LC_MONETARY, 'en_US.UTF-8');
			echo money_format('%.2n', $number);
		}
	
	?>
	
	<p>4. Create a function that will accept a number and display it as a formatted number. Use 1234567890 for your testing.</p>
	<?php formatNumber(); ?>
	
	<p>5. Create a function that will accept a number and display it as US currency.  Use 123456 for your testing.</p>
	<?php formatUsCurrency(); ?>
	
	<p><a href="https://bitbucket.org/hmsieck/intro-php/src/8ac3a7a39d9a/phpFunctions.php?at=master&fileviewer=file-view-default">View PHP Code</a></p>
	
	
	
</body>
</html>
