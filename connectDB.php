<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "wdv341";
$sql="";

try {
	
	global $event_name, $event_description, $event_presenter, $event_date, $event_time;
	
    $conn = new PDO("mysql:host=$servername;dbname=wdv341", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully <br>"; 
	
	$sql = "INSERT INTO wdv341_event (";
				$sql .= "event_name, ";
				$sql .= "event_description, ";
				$sql .= "event_presenter, ";
				$sql .= "event_date, ";
				$sql .= "event_time ";		//Last column doesnt have a comma after it.

				$sql .= ") VALUES (";
				$sql .= "'$event_name',";
				$sql .= "'$event_description',";
				$sql .= "'$event_presenter',";
				$sql .= "'$event_date', ";
				$sql .= "'$event_time' ";	//Last value doesnt have a comma after it.

				$sql .= ")";
				
	
	$stmt = $conn->prepare($sql);
    $stmt->bindParam(':event_name', $event_name);
    $stmt->bindParam(':event_description', $event_description);
    $stmt->bindParam(':event_presenter', $event_presenter);
	$stmt->bindParam(':event_date', $event_date);
	$stmt->bindParam(':event_time', $event_time);
    $stmt->execute();
	
	// echo "New Event Created Succesfully";
    }

catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
?>