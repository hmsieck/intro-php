<?php
	//Get the Event data from the server.

	include 'connectPDO.php';

	$sql = "SELECT event_id, event_name, event_description, event_presenter, event_day, event_time FROM wdv341_events";
	
	$stmt = $conn -> prepare($sql);

	$stmt -> execute();

	
	
	

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>WDV341 Intro PHP  - Display Events Example</title>
    <style>
		.eventBlock{
			width:50%;
			margin-left:auto;
			margin-right:auto;
			background-color:#CCC;	
		}
		
		.displayEvent{
			text_align:left;
			font-size:18px;	
		}
		
		.displayDescription {
			margin-left:100px;
		}
	</style>
</head>

<body>
    <h1>WDV341 Intro PHP</h1>
    <h2>Example Code - Display Events as formatted output blocks</h2>   
    <h3> <?php echo $row->num_rows; ?> Events are available today.</h3>

<?php
	
	
	//Display each row as formatted output
	while( $row = $stmt->fetch(PDO::FETCH_ASSOC))		
	//Turn each row of the result into an associative array 
  	{
		$name = $row['event_name'];
		$description = $row['event_description'];
		$presenter = $row['event_presenter'];
		$time = $row['event_time'];
		$date = $row['event_day'];
		$formatDate = date("m-d-Y", strtotime($date));
		
		echo $newDate;
		//echo $row['event_id'];

		//For each row you have in the array create a block of formatted text
?>
	<p>
        <div class="eventBlock">	
            <div>
            	<span class="displayEvent">Event: <?php echo $name;?></span>
            	<span class="displayDescription">Description: <?php echo $description;?></span>
            </div>
            <div>
				<span class="displayPresenter">Presenter:<?php echo $presenter;?></span>
            </div>
            <div>
            	<span class="displayTime">Time:<?php echo $time;?></span>
            </div>
            <div>
            	<span class="displayDate">Date:<?php echo $formatDate;?></span>
            </div>
        </div>
    </p>

<?php
  	}//close while loop
	$row->close();
	$conn->close();	//Close the database connection	
?>
</div>	
</body>
</html>