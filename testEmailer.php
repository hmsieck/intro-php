<!doctype HTML>


<html>
	
	<head><title>PHP Test Emailer</title></head>
	
	<body>
		<h1>Unit 5 assignment: Email Class </h1>
		<?php		//mail function doesn't run on local server


			include 'Emailer.php';  //linking external file

			$newEmail = new Emailer(); //instantiate a new object/variable -- creating a new object from the class file 

			$newEmail->setSendTo("hmsieck@dmacc.edu"); //associating this object with method we are trying to call 
			echo "<p> <strong>Send To:</strong> " . $newEmail->getSendTo() . "</p>";

			$newEmail->setSentFrom("contact@hollysieck.info");
			echo "<p><strong>Sent From:</strong> " . $newEmail->getSentFrom() . "</p>";

			$newEmail->setEmailSubject("Test Subject");
			echo "<p> <strong>Email Subject:</strong> " . $newEmail->getEmailSubject() . "</p>";
	

			$newEmail->setEmailMsg("Lorem & ipsum dolor sit amet, consectetur adipiscing elit. In massa libero, mattis quis justo eu, luctus congue dolor. Proin metus nulla, feugiat in ipsum sed, consectetur vulputate dolor. Duis non turpis et est laoreet suscipit. Cras quis ligula sed mauris finibus facilisis. Maecenas in mollis quam. Aliquam erat volutpat. Sed odio neque, facilisis ut semper quis, pellentesque ut tellus. Nullam fermentum ligula quis sapien rutrum, ultricies aliquam lectus pretium. Curabitur pretium, ante vel suscipit commodo, quam magna eleifend tortor, sed congue elit dui non elit. In imperdiet sit amet est a fringilla. Vestibulum nec nisi rhoncus, tempor nulla eget, tristique massa. Nulla iaculis orci sed ornare hendrerit. In cursus, odio a consectetur vulputate, tortor sem tincidunt diam, ut fermentum lacus quam vitae purus. Sed quis mattis urna, non varius ipsum. In metus nibh, suscipit eget sem quis, porta tempor dolor. Mauris vitae velit sit amet justo pulvinar feugiat.");
			echo "<p> <strong>Email Message: </strong>" .  $newEmail->getEmailMsg() . "</p>";

			$newEmail->sendEmail(); //creates and sends an email

		?>
		
		<a href= "https://bitbucket.org/hmsieck/intro-php/src">View PHP code</a>
	</body>
</html>